# Mensagem #

Este projeto gera loga de execução de métodos para geraçao de gráficos de tempos de execução no Kibana.

Para rodar o projeto execute os seguintes passos:

1. Importe o projeto no STS Spring;
    Download em https://spring.io/tools/sts/all

2. Rode o projeto como Spring Boot

3. Chame a URL http://localhost:8080/executar para gerar logs de tempo de execução.

4. Baixe o Logstash:
   https://www.elastic.co/downloads/logstash

5. Baixe o ElasticSearch:
   https://www.elastic.co/downloads/elasticsearch

6. Baixe o Kibana
   https://www.elastic.co/downloads/kibana
   
7. Descompactar todos na pasta C:/Produtos/DevOps/Monitoring

8. Rodar o C:\Produtos\DevTools\Monitoring\elasticsearch-5.4.1\bin\elasticsearch.bat

9. Copiar o arquivo logstash.conf para Produtos\DevTools\Monitoring\logstash-5.4.1

10.Rodar o Logstah com C:\Produtos\DevTools\Monitoring\logstash-5.4.1\bin\logstash.bat -f logstash.conf

11 Rodar o Kibana com C:\Produtos\DevTools\Monitoring\kibana-5.4.1-windows-x86\bin\kibana.bat

