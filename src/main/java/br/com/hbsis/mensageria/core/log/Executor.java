package br.com.hbsis.mensageria.core.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Executor {
	
	static Logger LOGGER = LoggerFactory.getLogger(Executor.class);
	
	@Autowired
	Assina assinar;
	@Autowired
	Busca busca;
	@Autowired
	Consulta consulta;
	@Autowired
	Envio envio;
	@Autowired
	Monta montar;
	
	@RequestMapping("/executar")
	public String executar() {
		LOGGER.info("INFO");
		LOGGER.debug("DEBUG");
		LOGGER.warn("WARN");
		LOGGER.error("ERROR");
		LOGGER.trace("TRACE");
		
		StringBuilder sb = new StringBuilder("Starting controller executar");
		LOGGER.debug(sb.toString());
		try {
			sb.append(montar.montarLote()+" | ");
			sb.append(envio.envioNFe()+" | ");
			sb.append(consulta.consultaProtocolo()+" | ");
			sb.append(busca.buscaRetornoLote()+" | ");
			sb.append(assinar.assinarXML()+" | ");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		LOGGER.debug(sb.append("Finished controller executar").toString());
				
		return sb.toString();
	}

}
