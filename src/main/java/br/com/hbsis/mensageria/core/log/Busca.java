package br.com.hbsis.mensageria.core.log;

import java.util.Random;

import org.springframework.stereotype.Service;

@Service
public class Busca extends Base {
	
	public String buscaRetornoLote() throws InterruptedException {
		LOGGER.debug("Starting service buscaRetornoLote");
		Thread.sleep(new Random().nextInt(RANDOM_MAX)+RANDOM_MIN);
		LOGGER.debug("Finished service buscaRetornoLote");
		return "Busca Retorno do Lote.";		
	}
}
