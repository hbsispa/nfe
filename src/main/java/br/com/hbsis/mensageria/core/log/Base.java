package br.com.hbsis.mensageria.core.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Logback: https://logback.qos.ch/reasonsToSwitch.html
 * @author mohamed.jammal
 *
 */
public class Base {
	
	static Logger LOGGER = LoggerFactory.getLogger(Base.class);
	static final int RANDOM_MIN = 150;
	static final int RANDOM_MAX = 45000;
	
}
