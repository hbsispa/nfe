package br.com.hbsis.mensageria.core.log;

import java.util.Random;

import org.springframework.stereotype.Service;

@Service
public class Consulta  extends Base {
	
	public String consultaProtocolo() throws InterruptedException {
		LOGGER.debug("Starting service consultaProtocolo");
		Thread.sleep(new Random().nextInt(RANDOM_MAX)+RANDOM_MIN);
		LOGGER.debug("Finished service consultaProtocolo");
		return "Consultou o Protocolo";		
	}
}
