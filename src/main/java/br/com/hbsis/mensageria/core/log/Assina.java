package br.com.hbsis.mensageria.core.log;

import java.util.Random;

import org.springframework.stereotype.Service;

@Service
public class Assina  extends Base {
		
	public String assinarXML() throws InterruptedException {
		LOGGER.debug("Starting service assinarXML");
		Thread.sleep(new Random().nextInt(RANDOM_MAX)+RANDOM_MIN);
		LOGGER.debug("Finished service assinarXML");
		return "Assinou XML";		
	}
}
