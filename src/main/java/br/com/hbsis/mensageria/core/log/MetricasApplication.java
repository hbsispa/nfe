package br.com.hbsis.mensageria.core.log;

import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.core.util.StatusPrinter;

@SpringBootApplication
public class MetricasApplication  {

	public static void main(String[] args) {
		
		LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
        StatusPrinter.print(lc);

		SpringApplication.run(MetricasApplication.class, args);
	}
}
