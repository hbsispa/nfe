package br.com.hbsis.mensageria.core.log;

import java.util.Random;

import org.springframework.stereotype.Service;

@Service
public class Monta  extends Base {
	
	public synchronized String montarLote() throws InterruptedException {
		LOGGER.debug("Starting service montarLote");
		Thread.sleep(new Random().nextInt(RANDOM_MAX)+RANDOM_MIN);
		LOGGER.debug("Finished service montarLote");
		return "Montou o Lote";		
	}

}
