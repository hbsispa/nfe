package br.com.hbsis.mensageria.core.log;

import java.util.Random;

import org.springframework.stereotype.Service;

@Service
public class Envio extends Base {
	
	public String envioNFe() throws InterruptedException {
		LOGGER.debug("Starting service envioNFe");
		Thread.sleep(new Random().nextInt(RANDOM_MAX)+RANDOM_MIN);
		LOGGER.debug("Finished service envioNFe");
		return "Enviou a NFe";		
	}
}
